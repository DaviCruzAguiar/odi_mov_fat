
-- --------------------------------------------------
-- Autor: Davi Aguiar
-- Data: 08/01/2022
-- Projeto: MOV FAT
-- Ferramentas Utilizadas: ODI 12c e Oracle SqlDeveloper 21.4
-- --------------------------------------------------

-- ------------------------------- DESCRIÇÃO ---------------------------------- 

Projeto MOV_FAT para carga de arquivo .CSV com dados consolidados de movimentação de cartões. 
O arquivo contêm informações de cada movimentação feita por cartões de assosciados, como valor, data da movimentação, etc.

Obs.: Todos dados são fictícios, criados para projeto de desafio técnico ETL em processo seletivo da empresa DBC/Sicredi.


-- ------------------------------- ARQUIVOS ---------------------------------- 

-- Pasta "EVIDENCIAS": Contêm prints de todos os objetos desenvolvimentos para o projeto. 
	- Modelo ODI das tabelas
	- Modelo ODI do arquivo
	- Modelo SQL das tabelas
	- Package ODI
	- Mapeamento ODI
	- Execuções ODI

-- Pasta "FILES_ODI": Contêm todos os arquivos .XML de exportação dos objetos criados no ODI 12c.

-- Pasta "SCRIPTS": Contêm todos os arquivos .SQL dos scripts criados para a modelagem de banco de dados do projeto.

-- Arquivo "MOVIMENTO_FLAT.csv": Arquivo gerado pelo ODI como resultado da carga ETL realizada pela execução do projeto.

-- ------------------------------- COMENTÁRIOS ----------------------------------

Optei pelo design implementado, pois é o padrão que utilizo atualmente em meus desenvolvimentos.

É importante comentar que alguns detalhes foram alterados e/ou ignorados priorizando uma implementação mais rápida.Dentre elas padrões de nomenclatura, criação de novo projeto, novas topologias, etc.

Com mais tempo de desenvolvimento, seriam seguidas todas as boas práticas de desenvolvimento e padrões sugeridos pela arquitetura do cliente. Visando boa manutenção e desempenho de performance do projeto.





